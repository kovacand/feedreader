package cz.cvut.kovacand.feedreader.util

import android.content.res.Resources
import android.util.DisplayMetrics

class ResUtils {

    companion object {

        fun dp2px(res: Resources, dp: Float) : Float {
            return dp * (res.displayMetrics.density / 160f)
        }

    }


}