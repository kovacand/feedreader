package cz.cvut.kovacand.feedreader

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cz.cvut.kovacand.feedreader.ui.details.DetailsFragment

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.details_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_list, DetailsFragment.newInstance(intent.getLongExtra(App.KEY_ARTICLE_ID, -1L)))
                .commitNow()
        }
    }

}
