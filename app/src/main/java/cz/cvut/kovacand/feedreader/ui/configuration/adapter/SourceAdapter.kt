package cz.cvut.kovacand.feedreader.ui.configuration.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.kovacand.feedreader.R
import cz.cvut.kovacand.feedreader.model.entity.Source
import kotlinx.android.synthetic.main.list_item.view.*

class SourceAdapter(var data: MutableList<Source>, private val onClickListener: (View) -> Unit) : RecyclerView.Adapter<SourceAdapter.Holder>() {

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        itemView.setOnClickListener(onClickListener)
        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val regex = """\.[^.]*""".toRegex()
        holder.itemView.tv_title.text = regex.find(data[position].url)?.value?.substring(1) ?: "Source"

        holder.itemView.tv_description.text = data[position].url
        holder.itemView.contentDescription = data[position].id.toString()
    }

    override fun getItemCount() = data.size
}