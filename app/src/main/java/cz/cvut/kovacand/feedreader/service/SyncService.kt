package cz.cvut.kovacand.feedreader.service

import android.app.job.JobInfo
import android.app.job.JobParameters
import android.app.job.JobScheduler
import android.app.job.JobService
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import cz.cvut.kovacand.feedreader.App
import cz.cvut.kovacand.feedreader.model.entity.Article
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SyncService : JobService() {

    private var isRunning = false

    override fun onStartJob(params: JobParameters?): Boolean {
        App.d("job started")
        isRunning = true
        doJob(params)
        baseContext.sendBroadcast(Intent(App.ACTION_JOB_STARTED))
        return isRunning
    }

    private fun doJob(params: JobParameters?) {
        val coroutineScope = CoroutineScope(Dispatchers.Main + Job())
        coroutineScope.launch(Dispatchers.Main) {
            val repo = App.provideFeedRepository()
            val sources = repo.getSourceDao().getSources()
            val articles = mutableListOf<Article>()
            sources.forEach { articles.addAll(repo.remoteRepository.getFeed(it)) }
            repo.getArticleDao().clear()
            articles.forEach { repo.getArticleDao().addArticle(it) }
            isRunning = false
            jobFinished(params, isRunning)
            baseContext.sendBroadcast(Intent(App.ACTION_JOB_FINISHED))
            App.d("job finished")
        }
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        jobFinished(params, isRunning)
        return isRunning
    }

    companion object {
        private const val JOB_ID = 255
        private const val ONE_HOUR = 30L * 60L * 1000L
        fun schedule(context: Context?) {
            App.d("scheduled")
            val scheduler = context?.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
            scheduler.schedule(
                JobInfo.Builder(JOB_ID, ComponentName(context, SyncService::class.java))
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPersisted(true)
                    .setPeriodic(ONE_HOUR)
                    .build()
            )
        }
        fun runImmediately(context: Context?) {
            App.d("immediate run")
            val scheduler = context?.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
            scheduler.schedule(
                JobInfo.Builder(JOB_ID, ComponentName(context, SyncService::class.java))
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setMinimumLatency(1)
                    .setOverrideDeadline(1)
                    .build()
            )
        }

        fun isRunning(context: Context?) : Boolean {
            val scheduler = context?.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return scheduler.getPendingJob(JOB_ID) != null
            }
            for (job in scheduler.allPendingJobs) {
                if (job.id == JOB_ID) {
                    return true
                }
            }
            return false
        }

        fun cancel(context: Context?) {
            App.d("canceled")
            val scheduler = context?.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
            scheduler.cancel(JOB_ID)
        }
    }


}