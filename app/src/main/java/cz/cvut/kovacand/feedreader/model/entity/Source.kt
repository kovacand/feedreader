package cz.cvut.kovacand.feedreader.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Source(@PrimaryKey val id: Long, val url: String) // int?