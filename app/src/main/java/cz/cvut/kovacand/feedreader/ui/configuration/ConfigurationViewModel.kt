package cz.cvut.kovacand.feedreader.ui.configuration

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.cvut.kovacand.feedreader.App
import cz.cvut.kovacand.feedreader.model.entity.Source
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ConfigurationViewModel : ViewModel() {

    private lateinit var sourcesLiveList: MutableLiveData<MutableList<Source>>

    fun getSources() : MutableLiveData<MutableList<Source>> {
        if (!::sourcesLiveList.isInitialized) {
            sourcesLiveList = MutableLiveData()
            fetchSources()
        }
        return sourcesLiveList
    }

    fun addSource(url: String) {
        App.provideFeedRepository().getSourceDao().addSource(Source(System.currentTimeMillis(), url))
        fetchSources()
    }

    fun removeSourceById(id: Long) {
        App.provideFeedRepository().getSourceDao().removeSourceById(id)
        fetchSources() // TODO zmazat len clanky
    }

    private fun fetchSources() {
        val coroutineScope = CoroutineScope(Dispatchers.Main + Job())
        coroutineScope.launch {
            sourcesLiveList.postValue(App.provideFeedRepository().getSourceDao().getSources().toMutableList())
        }
    }

}
