package cz.cvut.kovacand.feedreader.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.kovacand.feedreader.R
import cz.cvut.kovacand.feedreader.model.entity.Article
import kotlinx.android.synthetic.main.list_item.view.*

class ItemAdapter(var data: MutableList<Article>, private val onClickListener: (View) -> Unit) : RecyclerView.Adapter<ItemAdapter.Holder>() {

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        itemView.setOnClickListener(onClickListener)
        return Holder(itemView)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.itemView.tv_title.text = data[position].title
        holder.itemView.tv_description.text = data[position].description
        holder.itemView.contentDescription = data[position].id.toString()
    }

    override fun getItemCount() = data.size
}