package cz.cvut.kovacand.feedreader.ui.configuration

import android.app.Activity
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.kovacand.feedreader.R
import cz.cvut.kovacand.feedreader.ui.configuration.adapter.SourceAdapter
import kotlinx.android.synthetic.main.configuration_fragment.*

class ConfigurationFragment : Fragment() {

    companion object {
        fun newInstance() = ConfigurationFragment()
    }

    private lateinit var viewModel: ConfigurationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.configuration_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_sources.layoutManager = LinearLayoutManager(context)
        rv_sources.itemAnimator = DefaultItemAnimator()
        rv_sources.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ConfigurationViewModel::class.java)
        viewModel.getSources().observe(this, Observer {sources ->
            val adapter = SourceAdapter(sources) {
                AlertDialog.Builder(context!!)
                    .setTitle(R.string.remove_source_)
                    .setPositiveButton(android.R.string.yes) { dialog, _ ->
                        viewModel.removeSourceById(it.contentDescription.toString().toLong())
                        dialog.dismiss()
                        activity?.setResult(Activity.RESULT_OK)
                    }.show()
            }
            rv_sources.adapter = adapter
            adapter.notifyDataSetChanged()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_configuration, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.i_add -> {
                showAddDialog()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showAddDialog() {
        val et = EditText(context)
        et.inputType = InputType.TYPE_TEXT_VARIATION_URI
        AlertDialog.Builder(context!!)
            .setTitle(R.string.enter_url)
            .setView(et)
            .setPositiveButton(R.string.add_source) { dialog, _ ->
                viewModel.addSource(et.text.toString())
                dialog.dismiss()
                activity?.setResult(Activity.RESULT_OK)
            }.show()
    }

}
