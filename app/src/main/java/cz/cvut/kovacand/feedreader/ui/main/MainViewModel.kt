package cz.cvut.kovacand.feedreader.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.cvut.kovacand.feedreader.App
import cz.cvut.kovacand.feedreader.model.entity.Article


class MainViewModel : ViewModel() {

    private lateinit var articleListLive: MutableLiveData<MutableList<Article>>
    var isLoaded = false

    fun getArticles(): MutableLiveData<MutableList<Article>> {
        if (!::articleListLive.isInitialized) {
            articleListLive = MutableLiveData()
            updateArticles()
        }
        return articleListLive
    }

    fun updateArticles() {
        articleListLive.postValue(App.provideFeedRepository().getArticleDao().getArticles().toMutableList())
        isLoaded = true
    }

    override fun onCleared() {
        super.onCleared()
    }
}
