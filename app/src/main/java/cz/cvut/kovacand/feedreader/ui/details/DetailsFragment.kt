package cz.cvut.kovacand.feedreader.ui.details

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.webkit.WebSettings
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import cz.cvut.kovacand.feedreader.App
import cz.cvut.kovacand.feedreader.R
import kotlinx.android.synthetic.main.details_fragment.*


class DetailsFragment : Fragment() {

    companion object {
        fun newInstance(articleId: Long) : Fragment {
            val fragment = DetailsFragment()
            val args = Bundle()
            args.putLong(App.KEY_ARTICLE_ID, articleId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    private lateinit var viewModel: DetailsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        wv_content.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val articleId = arguments?.getLong(App.KEY_ARTICLE_ID) ?: -1L
        viewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)
        //if (!viewModel.isLoaded) {
            viewModel.getPage(articleId).observe(this, Observer {
                wv_content.loadDataWithBaseURL("", it, "text/html", "UTF-8", "")
            })
        //}
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_details, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.i_share -> {
                if (viewModel.article.id == -1L) {
                    Toast.makeText(context, R.string.no_article_to_share, Toast.LENGTH_SHORT).show()
                } else {
                    val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, viewModel.article.title)
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "${viewModel.article.title}\r\n\r\n${viewModel.article.description}\r\n\r\n${viewModel.article.url}")
                    shareIntent.type = App.ARTICLE_MIME_TYPE
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.share_article)))
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
