package cz.cvut.kovacand.feedreader.model.repository.feed.remote

import com.prof.rssparser.Parser
import cz.cvut.kovacand.feedreader.model.entity.Article
import cz.cvut.kovacand.feedreader.model.entity.Source
import cz.cvut.kovacand.feedreader.model.mapper.ArticleMapper

class FeedRemoteRepository {

    private val parser = Parser()

    suspend fun getFeed(source: Source) : MutableList<Article> {
        return ArticleMapper.toArticleList(parser.getArticles(source.url))
    }

}