package cz.cvut.kovacand.feedreader

import android.app.Application
import android.content.Context
import android.util.Log
import cz.cvut.kovacand.feedreader.model.entity.Source
import cz.cvut.kovacand.feedreader.model.repository.feed.FeedRepository
import cz.cvut.kovacand.feedreader.service.SyncService

class App : Application() {

    companion object {

        const val KEY_ARTICLE_ID = "key_article_id"
        const val KEY_FIRST_LAUNCH = "key_first_launch"
        const val KEY_SYNC_ON_BACKGROUND = "key_sync_on_background"

        val SAMPLE_FEED_URLS = arrayOf(
            "https://www.androidauthority.com/feed"
            //"http://servis.idnes.cz/rss.aspx?c=technet",
            //"http://android-developers.blogspot.com/atom.xml"
        )
        const val ARTICLE_MIME_TYPE = "text/*"
        const val PREFS_NAME = "prefs.xml"

        private lateinit var instance: App

        private lateinit var feedRepository: FeedRepository
        fun provideFeedRepository(): FeedRepository {
            if (!::feedRepository.isInitialized) feedRepository = FeedRepository.getInstance(instance.baseContext)
            return feedRepository
        }

        fun d(s: String) {
            Log.d("log debug", s)
        }

        const val ACTION_JOB_STARTED = "cz.cvut.kovacand.feedreader.ACTION_JOB_STARTED"
        const val ACTION_JOB_FINISHED = "cz.cvut.kovacand.feedreader.ACTION_JOB_FINISHED"

    }

    init {
        instance = this
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        val shp = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        if (!shp.contains(KEY_FIRST_LAUNCH)) {
            val dao = provideFeedRepository().getSourceDao()
            SAMPLE_FEED_URLS.forEach { dao.addSource(Source(System.currentTimeMillis(), it)) }
            shp.edit().putBoolean(KEY_FIRST_LAUNCH, false).apply()
        }
        if (shp.getBoolean(KEY_SYNC_ON_BACKGROUND, false)) {
            SyncService.schedule(this)
        }
    }

}