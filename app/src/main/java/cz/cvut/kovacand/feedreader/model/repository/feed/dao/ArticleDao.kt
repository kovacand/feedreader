package cz.cvut.kovacand.feedreader.model.repository.feed.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import cz.cvut.kovacand.feedreader.model.entity.Article

@Dao
interface ArticleDao {
    @Insert
    fun addArticle(article: Article)

    @Query("SELECT * FROM Article WHERE id = :id")
    fun getArticle(id: Long) : Article

    @Query("SELECT * FROM Article WHERE 1")
    fun getArticles() : List<Article>

    @Query("DELETE FROM Article WHERE 1")
    fun clear()
}