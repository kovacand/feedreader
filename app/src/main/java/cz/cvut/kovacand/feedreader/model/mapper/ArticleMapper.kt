package cz.cvut.kovacand.feedreader.model.mapper

import cz.cvut.kovacand.feedreader.model.entity.Article

class ArticleMapper {

    companion object {

        fun toArticleList(feedList: MutableList<com.prof.rssparser.Article>) : MutableList<Article> {
            val articleList = mutableListOf<Article>()
            var id = System.currentTimeMillis()
            for (a in feedList) {
                articleList.add(
                    Article(id++, a.title ?: "", a.description ?: "", a.author, a.content, a.link, a.pubDate, a.image))
            }
            return articleList
        }


    }

}