package cz.cvut.kovacand.feedreader

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cz.cvut.kovacand.feedreader.service.SyncService
import kotlinx.android.synthetic.main.settings_activity.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        val shp = getSharedPreferences(App.PREFS_NAME, Context.MODE_PRIVATE)
        cb_sync.isChecked = shp.getBoolean(App.KEY_SYNC_ON_BACKGROUND, false)
        cb_sync.setOnCheckedChangeListener { _, isChecked ->
            shp.edit().putBoolean(App.KEY_SYNC_ON_BACKGROUND, isChecked).apply()
            if (isChecked) SyncService.schedule(this) else SyncService.cancel(this)
        }
    }
}
