package cz.cvut.kovacand.feedreader

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import cz.cvut.kovacand.feedreader.ui.details.DetailsFragment
import cz.cvut.kovacand.feedreader.ui.main.MainFragment


class MainActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CODE = 1
    }

    private var frameDetails : FrameLayout? = null
    var isTabletMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        frameDetails = findViewById(R.id.container_detail)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(R.id.container_list, MainFragment.newInstance()).commitNow()
        }
        isTabletMode = frameDetails != null
        if (isTabletMode) {
            supportFragmentManager.beginTransaction().replace(R.id.container_detail, DetailsFragment.newInstance(-1)).commitNow()
        }
        window.addFlags(128)
        //setupResizeView()
    }
    /*
    private fun setupResizeView() {
        val resizeView = findViewById<View>(R.id.v_resize) ?: return
        resizeView.setOnTouchListener { _, event ->
            when(event.action) {
                MotionEvent.ACTION_DOWN -> {
                    true
                }
                MotionEvent.ACTION_MOVE -> {
                    true
                }
                else -> false
            }
        }
    }*/

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) when (item.itemId) {
            R.id.i_configure_feeds -> startActivityForResult(
                Intent(this, ConfigurationActivity::class.java),
                REQUEST_CODE
            )
            R.id.i_preferences -> startActivity(Intent(this, SettingsActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        supportFragmentManager.fragments.forEach { it.onActivityResult(requestCode, resultCode, data) }
    }



}
