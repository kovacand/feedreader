package cz.cvut.kovacand.feedreader.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Article(
    @PrimaryKey val id: Long,
    val title: String,
    val description: String,
    val author: String? = null,
    val content: String? = null,
    val url: String? = null,
    val date: String? = null,
    val image: String? = null)