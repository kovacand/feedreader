package cz.cvut.kovacand.feedreader.ui.main

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.*
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.kovacand.feedreader.App
import cz.cvut.kovacand.feedreader.DetailsActivity
import cz.cvut.kovacand.feedreader.MainActivity
import cz.cvut.kovacand.feedreader.R
import cz.cvut.kovacand.feedreader.service.SyncService
import cz.cvut.kovacand.feedreader.ui.details.DetailsFragment
import cz.cvut.kovacand.feedreader.ui.main.adapter.ItemAdapter
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private var refreshItem: MenuItem? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_content.layoutManager = LinearLayoutManager(context)
        rv_content.itemAnimator = DefaultItemAnimator()
        rv_content.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_main_frag, menu)
        refreshItem = menu.findItem(R.id.i_refresh)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.i_refresh -> SyncService.runImmediately(context)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.getArticles().observe(this, Observer { articles ->
            val adapter = ItemAdapter(mutableListOf()) {
                val id = it.contentDescription.toString().toLong()
                if (activity is MainActivity && (activity as MainActivity).isTabletMode) {
                        activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.container_detail, DetailsFragment.newInstance(id))?.commit()
                } else {
                    val intent = Intent(activity, DetailsActivity::class.java)
                    intent.putExtra(App.KEY_ARTICLE_ID, id)
                    startActivity(intent)
                }
            }
            rv_content.adapter = adapter
            adapter.data = articles
            adapter.notifyDataSetChanged()
        })
        if (SyncService.isRunning(context)) {
            App.d("is running")
            //showActionView()
        } else if (!viewModel.isLoaded) {
            App.d("not loaded")
            SyncService.runImmediately(context)
        }
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter()
        filter.addAction(App.ACTION_JOB_STARTED)
        filter.addAction(App.ACTION_JOB_FINISHED)
        activity?.registerReceiver(receiver, filter)
        if (SyncService.isRunning(context)) {
            App.d("is running")
            // showActionView()
        }
    }

    override fun onPause() {
        super.onPause()
        activity?.unregisterReceiver(receiver)
    }

    private val receiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                App.ACTION_JOB_STARTED -> showActionView()
                App.ACTION_JOB_FINISHED -> {
                    viewModel.updateArticles()
                    hideActionView()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MainActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            App.d("src changed, run")
            SyncService.runImmediately(context)
        }
    }

    private fun showActionView() {
        refreshItem?.actionView = ProgressBar(context)
    }

    private fun hideActionView() {
        refreshItem?.actionView = null
    }
}
