package cz.cvut.kovacand.feedreader.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.cvut.kovacand.feedreader.App
import cz.cvut.kovacand.feedreader.model.entity.Article

class DetailsViewModel : ViewModel() {

    private lateinit var page: MutableLiveData<String>
    lateinit var article: Article
    var isLoaded = false

    fun getPage(id: Long) : MutableLiveData<String> {
        if (!::page.isInitialized) {
            page = MutableLiveData()
            if (id == -1L) {
                article = Article(-1, "", "")
                isLoaded = true
                page.postValue( "<h3>Choose article</h3>")
            } else {
                article = App.provideFeedRepository().getArticleDao().getArticle(id)
                var data = "<h3>${article.title}</h3>"
                data += "${article.date} by ${article.author}<br>"
                data += "<a href=\"${article.url}\">View Original</a><br>"
                data += "<p>${article.description}</p><hr>"
                data += "<img width=\"100%\" height=\"auto\" src=\"${article.image}\"><hr>"
                data += article.content?.replace("<img[^>]*>".toRegex(), "")
                isLoaded = true
                page.postValue(data)
            }
        }
        return page
    }






}
