package cz.cvut.kovacand.feedreader.model.repository.feed.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import cz.cvut.kovacand.feedreader.model.entity.Source

@Dao
interface SourceDao {
    @Insert
    fun addSource(source: Source)

    @Query("SELECT * FROM Source WHERE 1")
    fun getSources() : List<Source>

    @Delete
    fun removeSource(source: Source)

    @Query("DELETE FROM Source WHERE id = :id")
    fun removeSourceById(id: Long)
}