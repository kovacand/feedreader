package cz.cvut.kovacand.feedreader.ui.main

interface ItemClickListener {
    fun onArticleSelected(id: Long)
}