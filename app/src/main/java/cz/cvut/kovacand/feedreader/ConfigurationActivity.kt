package cz.cvut.kovacand.feedreader

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cz.cvut.kovacand.feedreader.ui.configuration.ConfigurationFragment

class ConfigurationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.configuration_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_list, ConfigurationFragment.newInstance())
                .commitNow()
        }
    }

}
