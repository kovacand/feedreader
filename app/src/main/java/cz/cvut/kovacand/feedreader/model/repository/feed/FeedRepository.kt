package cz.cvut.kovacand.feedreader.model.repository.feed

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cz.cvut.kovacand.feedreader.model.entity.Article
import cz.cvut.kovacand.feedreader.model.entity.Source
import cz.cvut.kovacand.feedreader.model.repository.feed.dao.ArticleDao
import cz.cvut.kovacand.feedreader.model.repository.feed.dao.SourceDao
import cz.cvut.kovacand.feedreader.model.repository.feed.remote.FeedRemoteRepository

@Database(entities = [Article::class, Source::class], version = 1)
abstract class FeedRepository : RoomDatabase() {
    abstract fun getArticleDao(): ArticleDao
    abstract fun getSourceDao() : SourceDao

    val remoteRepository = FeedRemoteRepository()

    companion object {

        fun getInstance(context: Context) : FeedRepository {
            return Room.databaseBuilder(context, FeedRepository::class.java, "db").allowMainThreadQueries().build()
        }
    }


}